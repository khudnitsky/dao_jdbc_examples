package com.netcracker.ec.khudnitsky.lect_03.dao.converters;

import com.netcracker.ec.khudnitsky.lect_03.dao.dto.ClientDTO;
import com.netcracker.ec.khudnitsky.lect_03.dao.entities.Address;
import com.netcracker.ec.khudnitsky.lect_03.dao.entities.Client;

public class ClientConverter {

  public ClientDTO convert(Client client) {
    ClientDTO clientDTO = new ClientDTO();

    clientDTO.setId(client.getId());
    clientDTO.setFirstName(client.getFirstName());
    clientDTO.setLastName(client.getLastName());
    clientDTO.setBonusPoints(client.getBonusPoints());

    return clientDTO;
  }

  public Client convert(ClientDTO clientDTO) {
    Client client = new Client();

    client.setId(clientDTO.getId());
    client.setFirstName(clientDTO.getFirstName());
    client.setLastName(clientDTO.getLastName());
    client.setBonusPoints(clientDTO.getBonusPoints());

    Address address = new Address();
    address.setCity(clientDTO.getCity());
    address.setStreet(clientDTO.getStreet());
    client.setAddress(address);

    return client;
  }
}
