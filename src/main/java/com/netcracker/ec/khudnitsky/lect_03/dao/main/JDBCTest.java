package com.netcracker.ec.khudnitsky.lect_03.dao.main;

import com.netcracker.ec.khudnitsky.lect_03.dao.dto.ClientDTO;
import com.netcracker.ec.khudnitsky.lect_03.dao.services.ClientService;
import com.netcracker.ec.khudnitsky.lect_03.jdbc.DatabaseConnector;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class JDBCTest {

  static {
    Connection connection = null;
    try {
      connection = DatabaseConnector.getConnection();
      initialize(connection);
    } catch (SQLException e) {
      e.printStackTrace();

    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void initialize(Connection connection) throws SQLException {
    Statement createTableStatement = connection.createStatement();
    createTableStatement.execute("CREATE TABLE IF NOT EXISTS clients("
        + "id integer primary key, "
        + "first_name varchar(100),"
        + "last_name varchar(100),"
        + "bonus_points integer);");

    createTableStatement.execute("CREATE TABLE if not exists addresses("
        + "client_id integer REFERENCES clients (id),"
        + "city VARCHAR,"
        + "street VARCHAR)");
  }

  public static void main(String[] args) {
    ClientDTO newClient = new ClientDTO(125, "Alexei", "Khudnitsky", 44, "Minsk", "Dzerzhinskogo");
    ClientService clientService = new ClientService();
    clientService.addClient(newClient);

    List<ClientDTO> clients = clientService.getClients();
    clients.forEach(System.out::println);
  }
}
