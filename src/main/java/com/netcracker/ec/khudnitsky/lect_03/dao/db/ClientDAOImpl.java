package com.netcracker.ec.khudnitsky.lect_03.dao.db;

import com.netcracker.ec.khudnitsky.lect_03.dao.entities.Client;
import com.netcracker.ec.khudnitsky.lect_03.jdbc.DatabaseConnector;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ClientDAOImpl implements ClientDAO {

    private static final String SQL_QUERY_ADD_CLIENT = "INSERT INTO clients (id, first_name,last_name) VALUES (?,?,?)";
    private static final String SQL_QUERY_ADD_ADDRESS = "INSERT INTO addresses (client_id, city, street) VALUES (?,?,?)";
    private static final String SQL_QUERY_GET_CLIENTS = "SELECT * FROM clients";

    public void addClient(Client client) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DatabaseConnector.getConnection();

            preparedStatement = connection
                    .prepareStatement(SQL_QUERY_ADD_CLIENT);
            preparedStatement.setInt(1, client.getId());
            preparedStatement.setString(2, client.getFirstName());
            preparedStatement.setString(3, client.getLastName());
            preparedStatement.executeUpdate();

            preparedStatement = connection
                .prepareStatement(SQL_QUERY_ADD_ADDRESS);
            preparedStatement.setInt(1, client.getId());
            preparedStatement.setString(2, client.getAddress().getCity());
            preparedStatement.setString(3, client.getAddress().getStreet());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("SQL exception occurred during add client");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.out.println("SQL exception occurred during add client");
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Client> getClients() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        List<Client> clients = null;
        try {
            connection = DatabaseConnector.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_QUERY_GET_CLIENTS);
            clients = initClients(resultSet);

        } catch (SQLException e) {
            System.out.println("SQL exception occurred during add client");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                System.out.println("SQL exception occurred during add client");
                e.printStackTrace();
            }
        }
        return clients;
    }

    private List<Client> initClients(ResultSet resultSet) throws SQLException {
        List<Client> clients = new ArrayList<Client>();
        while (resultSet.next()) {
            Client client = new Client();
            client.setId(resultSet.getInt(1));
            client.setFirstName(resultSet.getString(2));
            client.setLastName(resultSet.getString(3));
            client.setBonusPoints(resultSet.getInt(4));
            clients.add(client);
        }
        return clients;
    }
}
