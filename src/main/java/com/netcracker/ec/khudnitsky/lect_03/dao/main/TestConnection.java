package com.netcracker.ec.khudnitsky.lect_03.dao.main;

import com.netcracker.ec.khudnitsky.lect_03.jdbc.DatabaseConnector;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestConnection {

  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    Connection conn = DatabaseConnector.getConnection();
    if (conn == null) {
      System.out.println("Нет соединения с БД!");
      System.exit(0);
    }
    Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery("SELECT * FROM clients");
    while (rs.next()) {
      System.out.println(rs.getRow() + ". " + rs.getString("first_name")
          + "\t" + rs.getString("last_name"));
    }
    stmt.close();
  }
}

