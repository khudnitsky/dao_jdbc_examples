package com.netcracker.ec.khudnitsky.lect_03.dao.entities;

public class Client {

  private int id;
  private String firstName;
  private String lastName;
  private int bonusPoints;
  private Address address;

  public Client() {
  }

  public Client(int id, String firstName, String lastName, int bonusPoints, Address address) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.bonusPoints = bonusPoints;
    this.address = address;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String name) {
    this.firstName = name;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public int getBonusPoints() {
    return bonusPoints;
  }

  public void setBonusPoints(int bonusPoints) {
    this.bonusPoints = bonusPoints;
  }

  @Override
  public String toString() {
    return "Client{" +
        "id=" + id +
        ", firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", bonusPoints=" + bonusPoints +
        ", address=" + address +
        '}';
  }

  public Address getAddress() {
    return address;
  }

  public void setAddress(Address address) {
    this.address = address;
  }
}
