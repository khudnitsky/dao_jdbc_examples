package com.netcracker.ec.khudnitsky.lect_03.dao.db;

import com.netcracker.ec.khudnitsky.lect_03.dao.entities.Client;
import java.util.List;

public interface ClientDAO {

  void addClient(final Client client);

  List<Client> getClients();
}
